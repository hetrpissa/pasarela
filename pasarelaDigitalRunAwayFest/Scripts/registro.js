﻿
function registro()
{
    var r = {}
        r.nombres='',
        r.apellidoPaterno='',
        r.apellidoMaterno='',
        r.dependencia='',
        r.correo = '',
        r.contrasena=''
    

    r.nombres = document.getElementById('nombres').value;
    r.apellidoPaterno = document.getElementById('apellidoPaterno').value;
    r.apellidoMaterno = document.getElementById('apellidoMaterno').value;
    r.dependencia = document.getElementById('dependencia').value;
    r.correo = document.getElementById('correo').value;
    r.contrasena = document.getElementById('contrasena').value;

    console.log(r);

    //VALIDAMOS QUE TODOS LOS CAMPOS ESTËN LLENOS
    if (r.nombres != '' && r.apellidoPaterno != '' && r.apellidoMaterno != '' && r.dependencia != '' && r.correo && r.contrasena!='')
    {
        if (ValidateEmail(r.correo)) {
            //AJAX     
            $.ajax({
                type: "POST",
                //url: '@(Url.Action("Registro", "Registro"))',
                url:'Home/Registro',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(r),
                dataType: "json",
                success: function (data) {
                    LimpiarCampos();
                    alert("Registro existoso!");
                    document.getElementById('closeModal').click();
                },
                error: function (xhr, status, error) {
                    LimpiarCampos();
                    alert("Error");
                    document.getElementById('closeModal').click();
                    //alert(xhr.responseText);
                }
            });
        }
        else
        {
            return alert('Introduzca un correo válido');
        }
    }
    else
    {
        return alert('Ningun campo debe estar vacio.');
    }
  
}

function LimpiarCampos() {
    $('#nombres').val('');
    $('#apellidoPaterno').val('');
    $('#apellidoMaterno').val('');
    $('#dependencia').val('');
    $('#correo').val
    $('#contrasena').val('');
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function ValidateEmail(inputText) {
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (inputText.match(mailformat)) {
        //alert("Valid email address!");
        //document.form1.text1.focus();
        return true;
    }
    else {

        //document.form1.text1.focus();
        return false;
    }

}