﻿using pasarelaDigitalRunAwayFest.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace pasarelaDigitalRunAwayFest.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        [HttpPost]
        public JsonResult Registro(registro r)
        {
            string mensaje = "";

            string constr = ConfigurationManager.ConnectionStrings["pasarelaDB"].ConnectionString;
            string query = "INSERT INTO Registro (nombres, apellidoPaterno, apellidoMaterno, dependencia, correo, contrasena)" +
                           " VALUES ('" + r.nombres + "','" + r.apellidoPaterno + "','" + r.apellidoMaterno + "','" + r.dependencia +
                            "','" + r.correo + "','" + r.contrasena + "')";

            SqlConnection con = new SqlConnection(constr);

            try
            {
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();

                cmd.ExecuteNonQuery();
                mensaje = "Registro Existoso!";

            }
            catch (Exception ex)
            {
                mensaje = "Hubo un error al guardar el registro!. " + ex.Message.ToString();
            }
            finally
            {
                con.Close();
            }

            //return RedirectToAction("Index", "Home");
            return Json(mensaje);

        }
    }
}