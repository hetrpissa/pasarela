﻿/* Set the width of the side navigation to 250px */
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

/* Set the width of the side navigation to 0 */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}

function closeNavVideo(i) {
    document.getElementById("mySidenav").style.width = "0";
    var video = document.getElementById('video');
    
    switch (i) {
        case 1:
            video.src = "/Content/assets/videos/presentacion.mp4";
            break;
        case 2:
            video.src = "/Content/assets/videos/Neza2.mp4";
            break;
        case 3:

            video.src = "/Content/assets/videos/ModaSocialTendenciasNeza2.mp4";
            break;
        case 4:

            video.src = "/Content/assets/videos/IxtapalucaGIOROS.mp4";
            break;
        case 5:

            video.src = "/Content/assets/videos/Texcocovideodegraduaciondepatrones.mp4";
            break;
        case 6:

            video.src = "/Content/assets/videos/VideoTilapa.mp4";
            break;
    }
        


}